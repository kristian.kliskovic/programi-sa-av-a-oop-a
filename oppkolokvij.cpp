#include <iostream>
#include<ctime>
#include<cstdlib>
using namespace std;

/*class Ventil{	
	double maxprotok;
	double otvoren;
public:
	Ventil():maxprotok(0),otvoren(0){};
	Ventil(double a, double b):maxprotok(a),otvoren(b){};
	void zatvori(){                 	                
		otvoren=0;
    };
	void otvori(){
		otvoren=1;	
	};
	void postavi(double postotak){
		otvoren=postotak;
	};
	void maxprot(double a){
		maxprotok=a;
	};
	double otvorenost(){
		return otvoren;	
	};
	double getmaxprot(){
		return maxprotok;	
	};
	
};

bool veci(Ventil a, Ventil b){
	return (a.otvorenost()*a.getmaxprot())>(b.otvorenost()*b.getmaxprot());
};


int main() {
	srand((unsigned)time(NULL));
	Ventil v[5];
	for(int i=0;i<5;i++){
		v[i].maxprot((double)rand()/RAND_MAX*19+1);
		v[i].postavi((double)rand()/RAND_MAX);
	};
	int spremnik=8479;
	double ukprotok=0;
	for(int i=0;i<5;i++){
		ukprotok += v[i].otvorenost()*v[i].getmaxprot();
	};
	std::cout<<spremnik/ukprotok;
	return 0;
}*/


class Battery{
	friend ostream& operator<<(ostream&,Battery&);
	friend double punjenje(double,Battery&);
	double kapacitet;
	double trenkapacitet;
public:
	Battery():kapacitet(0),trenkapacitet(0){
	};
	Battery(double a): kapacitet(a),trenkapacitet(a){
	};	
	double getkapacitet(){
		return kapacitet;
	}
	void potrosi(double a){
		trenkapacitet-=a;
		if(trenkapacitet<=0)trenkapacitet=0;
	}
	double postotak(){
		return (trenkapacitet/kapacitet);
	}
	
	bool trebapunjenje(){
		if(postotak()<=0.15)return 1;
		return 0;	
	}
	
};

ostream& operator<<(ostream& izlaz, Battery& b){
	izlaz<<"Battery: "<<b.kapacitet<<"mAh ,"<<b.postotak()<<"% remaining";
	return izlaz;
}

double punjenje(double struja, Battery& b){
	struja*=0.8;
	return (b.kapacitet-b.trenkapacitet)/struja;
}

int main(){
	Battery b(3200);
	b.potrosi(1400);
	std::cout<<b<<endl;
	cout<<punjenje(1000,b)<<" hours until full charge!";
	
	
	
	
}
