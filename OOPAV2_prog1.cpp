#include<iostream>
#include<string>
#include<stdlib.h>
//konstruktor kopije kompjaler sam napravi bez da me pita jel mi treba
//ne radi nista pametno
//ako napisem parametarski konstruktor, a ne defaultni, kompajelr mi nece radit defaultni
using namespace std;
class Array{
	int mSize;
	int* mData;
public:
	Array(int size):mSize(size){
		//mData=(int*)calloc(size,sizeof(int));
		mData = new int[mSize];
		for(int z=0;z<mSize;z++) mData[z]=0;
	}
	
	Array(const Array& array1): mSize(array1.mSize){  //konstruktor kopije sa dubokim kopiranjem
		this->mData=new int[mSize];
		for(int i=0;i<mSize;i++){
			this->mData[i]=array1.mData[i];
		}
	}
	
	
	Array& operator= (const Array& array){
		if(this!=&array){
			
		}
	}
	
	
	int get(int index){return mData[index];}
	
	
	void set(int index, int value){
		mData[index]=value;
	}
	
	
	~Array(){r
		delete[] mData;
	}
};


int main(){
	//Array array, funnyArray;
	//Array array2(array); //konstruktor kopije, prepise sve stvari iz jednog objekta u drugi objekt
	//array=funnyArray;
	
	Array a(10);
	cout<<a.get(0)<<endl;
	Array b(a);  //plitko kopiranje, kopira i tu adresu i onda obje mDate pokazuju na isti koad memorije
				//kad dode destrukor on dvaput pokusa free-at isti komad memorije
	cout<<b.get(0)<<endl;
	
	a.set(0,222);
	cout<<a.get(0)<<endl;
	cout<<b.get(0)<<endl;
	
	return 0;
}
