#include<iostream>
using namespace std;
class Enemy{
protected:
    int mHp;
    int mDamage; //member
public:
    Enemy(int hp, int damage): mHp(hp), mDamage(damage){
        cout<<"ctor Enemy"<<endl;
    };
    int attack(){
        return mDamage;
    }
};

class Thief: public Enemy{
    int mGold;
public:
    Thief(int gold, int hp, int damage): mGold(gold), Enemy(hp, damage) {
        cout<<"ctor Thief"<<endl;
    }//ako se ne navede konstruktor Enemy, pozvat ce se defaultni, ali ako ne postoji onda fuck
    int attack(){
        mGold++;
        return mDamage;
    }
    int getGold() const{ return mGold;}


};


int main(){
    Enemy creep1(100,1), creep2(100,1);
    Thief t1(25,100,2);
    cout<<t1.getGold()<<endl;
    cout<<t1.attack()<<endl;
    cout<<t1.getGold()<<endl;
    Enemy* pT = &t1;
    //cout<<pT->getGold()<<endl; ovo ne ide
    cout<<pT->attack()<<endl; // sada nece povecat gold jer se pozove metoda iz Enemya
    cout<<t1.getGold()<<endl;
    

    
    return 0;
}