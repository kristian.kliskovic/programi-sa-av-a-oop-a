

//unarni operator se radi kao clanski(u klasi)

#include<iostream>
using namespace std;

class Complex{
	friend void print(const Complex& ref);
	friend bool operator== (const Complex& lhs, const Complex& rhs);
	friend ostream& operator<<(ostream& out,const Complex& rhs);
	friend istream& operator>>(istream& in, Complex& rhs);
		
	//private:
	int mRe, mIm;
	public:
	Complex(): mRe(0), mIm(0){} //cak i da smo prvo stavili mIm, opet bi se prvo postavio mRe
	Complex(int re, int im) : mRe(re), mIm(im){}
	void setRe(int re) {mRe=re;}
	void setIm(int im) {mIm=im;}
	int getRe(){
		return mRe;
	}
	int getIm(void){
		return mIm;
	}
	Complex& operator+= (Complex& rhs){
		this->mRe+=rhs.mRe;
		this->mIm+=rhs.mIm;
		return *this;
	}
};

bool operator==(const Complex& lhs, const Complex& rhs){
	return lhs.mRe==rhs.mRe&&lhs.mIm==rhs.mIm; // mozda se sve desno od && nece isvesti ako je lijevo False, a ako je funkcija s desne strane moze bit problem
}

void print(const Complex& ref){
	cout<<ref.mRe<<" i "<<ref.mIm;             //iako su ti podatci u private djelu ova funkcija im smije pristupiti
	return;
}

ostream& operator<<(ostream& out, const Complex& rhs){
	return out<<rhs.mRe<<" "<<rhs.mIm<<"i";   //vraca "tok"
}

istream& operator>>(istream& in, Complex& rhs){
	in>>rhs.mRe;
	in.ignore(1);
	in>>rhs.mIm;
	in.ignore(1);
	return in;
}


int main(){
	Complex c(10,11), d(19,11);   //stvoreno konstruktorom,ali skrivenim
	Complex* pC=new Complex(13,10);
	cin>>c;
	cin>>d;
	c+=*pC;
	print(c);
	
	
	
	return 0;
}
