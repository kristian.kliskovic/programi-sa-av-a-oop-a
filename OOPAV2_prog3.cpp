
#include<iostream>
#include<string>
#include<stdlib.h>
using namespace std;

class Animal{
	private:
		int age, weight;
	public:
		Animal(): age(0){
			cout<<"D ctor"<<endl;
		}
		Animal(int Age): age(Age){
			//lmao
		}
		virtual void makeNoise()=0; //cout<<"Noise. I am an animal"<<endl;
};
//moze postojati i virtulni desturktor
class Meerkat: public Animal{  //priv, prot ili public nacin nasljedivanja
		int minutesStared;
	public:
		Meerkat(): minutesStared(0), Animal(0){   //prvo se pozvao Animal konstruktor pa tek onda meerkat konstruktor
			cout<<"D ctor meerkat"<<endl;
		}
		void stare(){
			cout<<"Staring contest"<<endl;
		}
		void makeNoise() { cout<<"glasam se ko merkat"<<endl; }
		void sleep() {cout<<"zzzzzz"<<endl;}
};


int main(){
	//Animal r;
	Meerkat p1;
	p1.sleep();
}
