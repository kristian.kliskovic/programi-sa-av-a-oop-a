//nasljedivanje
//nad-klasa - zivotinja-tezina, glasaj_se, spavaj

//pod-klasa panda naslijedi klasu zivotinja

#include<iostream>
#include<string>
#include<stdlib.h>
using namespace std;

class Animal{
	private:
		int age, weight;
	public:
		Animal(): age(0){
			cout<<"D ctor"<<endl;
		}
		Animal(int Age): age(Age){
			//lmao
		}
		void makeNoise(){
			cout<<"Noise. I am an animal"<<endl;
			return;
		}
		void sleep(){
			cout<<"zzzzzz"<<endl;
			return;
		}
};

class Meerkat: public Animal{  //priv, prot ili public nacin nasljedivanja
		int minutesStared;
	public:
		Meerkat(): minutesStared(0), Animal(0){   //prvo se pozvao Animal konstruktor pa tek onda meerkat konstruktor
			cout<<"D ctor meerkat"<<endl;
		}
		void stare(){
			cout<<"Staring contest"<<endl;
		}
		void makeNoise() = 0 //cout<<"glasam se ko merkat"<<endl;
};

int main(){
	Meerkat m;
	m.makeNoise();
	m.stare();
	m.Animal::makeNoise();
	
	Animal* pani=&m;
	pani->sleep();
	
	Animal* animal = new Meerkat();
	
	//Animal a;
}
